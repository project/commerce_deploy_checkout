; Commerce Deploy Checkout Makefile

api = 2
core = 7.x

; Contribs

projects[addressfield][version] = 1.1
projects[addressfield][subdir] = contrib

projects[commerce_add_to_cart_confirmation][version] = 1.x
projects[commerce_add_to_cart_confirmation][subdir] = contrib
projects[commerce_add_to_cart_confirmation][download][type] = git
projects[commerce_add_to_cart_confirmation][download][branch] = 7.x-1.x
projects[commerce_add_to_cart_confirmation][download][revision] = ce82b9a

projects[commerce_checkout_progress][version] = 1.3
projects[commerce_checkout_progress][subdir] = contrib

projects[commerce_checkout_complete_registration][version] = 1.3
projects[commerce_checkout_complete_registration][subdir] = contrib

projects[commerce_checkout_redirect][version] = 2.0-rc1
projects[commerce_checkout_redirect][subdir] = contrib
